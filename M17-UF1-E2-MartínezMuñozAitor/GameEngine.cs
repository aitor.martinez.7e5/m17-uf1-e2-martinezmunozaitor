﻿using System;
namespace GameTools
{

    /*
     * GameEngine:
     * Protype to print sequence of frames in console.
     * 
     * To stop de main while press ESC key.
     * 
     */


    public class GameEngine
    {
        protected MyFirstProgram.MatrixRepresentation matrix = new();
        private ConsoleColor _backgroundConsoleColor;
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                //Ternari condition
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }


        //Declaració d'una propietat no protegida
        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }


        private void InitGame()
        {
            /*** Init variables ***/

            Frames = 0;
            engineSwitch = true;

            //Acces exemple with this:
            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;

            //Calculate the frame time in miliseconds. Time to refresh. F=1/s ->s=1/F
            time2liveframe = (int)((1 / _frameRate) * 1000);

            /*******/

            //Prepare Console
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;

            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");

            Start();

            System.Threading.Thread.Sleep(2000);
        }

        /*
         * Engine updates every frame
         * 
         * Reprint console
         */
        private void UpdateGame()
        {

            do
            {
                while (Console.KeyAvailable == false)
                {

                    CleanFrame();

                    Console.WriteLine($"Frame number {Frames}");

                    Update();

                    RefreshFrame();

                    CheckKeyboard4Engine();

                    Frames++;
                }

                cki = Console.ReadKey(true);

            } while (engineSwitch);

        }

        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key != ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {
            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit();
            Console.WriteLine(" Game Over. Closing game");
        }


        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop);
        }

        protected void Start()
        {
            matrix.clippingMatrix(matrix.blankMatrix());

        }

        protected void Update()
        {
            matrix.printMatrix();

            for (int i = matrix.TheMatrix.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = matrix.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                {
                    char temp = matrix.TheMatrix[i - 1, j];
                    matrix.TheMatrix[i - 1, j] = matrix.TheMatrix[i, j];
                    matrix.TheMatrix[i, j] = temp;
                }
            }

            

            Console.WriteLine("PULSA CUALQUIER TECLA PARA RESETEAR");
            Console.WriteLine("PULSA LA TECLA '9' PER L'EXERCICI");
            Console.WriteLine("PULSA DEL 0-8 PER FER L'EXERCICI BONUS");

            Random rnd = new();

            if (!char.IsDigit(cki.KeyChar))
            {
                for (int i = matrix.TheMatrix.GetLength(0) - 1; i >= 0; i--)
                {
                    for (int j = matrix.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                    {
                        matrix.TheMatrix[i, j] = '0';
                    }
                }
            }
  
            else if (cki.KeyChar.CompareTo('9') == 0)
            {
                for (int i = 0; i < matrix.TheMatrix.GetLength(0); i++)
                {
                    matrix.TheMatrix[0, i] = (char)('a' + rnd.Next(0, 26));
                }
            }

            else if (char.IsDigit(cki.KeyChar) && int.Parse(cki.KeyChar.ToString()) < matrix.TheMatrix.GetLength(0))
            {
                matrix.TheMatrix[0, int.Parse(cki.KeyChar.ToString())] = (char)('a' + rnd.Next(0, 26));
            }

        }

        protected void Exit()
        {
            //Code afer last frame

        }

    }
}


public class ConsoleSpiner
{
    int counter;
    public ConsoleSpiner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
        //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
}
